﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class AlumnosBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        //ALUMNO es la tabla del modelo, ALUMNOS es la tabla de la BD.
        public List<ALUMNO> obtenerAlumno()
        {
            return modelo.ALUMNOS.ToList();
        }
        
        public int agregarAlumno(ALUMNO pAlumno)
        {
            modelo.ALUMNOS.Add(pAlumno);
            return modelo.SaveChanges();
        }


        public ALUMNO buscarAlumno(ALUMNO pAlumno)
        {
            return modelo.ALUMNOS.SingleOrDefault(p => p.IdAlumno == pAlumno.IdAlumno);
        }

        public int modificarAlumno(ALUMNO pAlumno)
        {
            ALUMNO Alumno = buscarAlumno(pAlumno);

            Alumno.Nombre = pAlumno.Nombre;
            Alumno.Apellido = pAlumno.Apellido;
            Alumno.NumCarnet = pAlumno.NumCarnet;
            Alumno.Foto = pAlumno.Foto;
            Alumno.Edad = pAlumno.Edad;
            Alumno.FechaIngreso = pAlumno.FechaIngreso;
            Alumno.Direccion = pAlumno.Direccion;
            Alumno.HORARIO = pAlumno.HORARIO;
            Alumno.CARRERA = pAlumno.CARRERA;
            /// se le cambio a objeto entero
            return modelo.SaveChanges();
        }

        public int eliminarAlumno( ALUMNO pAlumno)
        {
            ALUMNO Alumno = buscarAlumno(pAlumno);
            if (Alumno != null)
                modelo.ALUMNOS.Remove(Alumno);

            return modelo.SaveChanges();
        }

        public ALUMNO obtenerAlumnoPorId(int pId)
        {
            return modelo.ALUMNOS.SingleOrDefault(p => p.IdAlumno == pId );
        }

        public ALUMNO buscarAlumnoPorNumcarnet(ALUMNO pAlumno)
        {
            return modelo.ALUMNOS.SingleOrDefault(p => p.NumCarnet == pAlumno.NumCarnet);
        }
    }
}
