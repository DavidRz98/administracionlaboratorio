﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class CarrerasBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        //CARRERA es la tabla del modelo, CARRERAS es la tabla de la BD.
        public List<CARRERA> obtenerCarrera()
        {
            return modelo.CARRERAS.ToList();
        }

        public int agregarCarrera(CARRERA pCarrera)
        {
            modelo.CARRERAS.Add(pCarrera);
            return modelo.SaveChanges();
        }

        public CARRERA buscarCarrera(CARRERA pCarrera)
        {
            return modelo.CARRERAS.SingleOrDefault(p => p.IdCarrera == pCarrera.IdCarrera);
        }

        public int modificarCarrera(CARRERA pCarrera)
        {
            CARRERA Carrera = buscarCarrera(pCarrera);

            Carrera.Nombre = pCarrera.Nombre;
            Carrera.DOCENTE = pCarrera.DOCENTE;
          
            return modelo.SaveChanges();
        }

        public int eliminarCarrera(CARRERA pCarrera)
        {
            CARRERA Carrera = buscarCarrera(pCarrera);
            if (Carrera != null)
                modelo.CARRERAS.Remove(Carrera);

            return modelo.SaveChanges();
        }

        public CARRERA obtenerCarreraPorId(int pId)
        {
            return modelo.CARRERAS.SingleOrDefault(p => p.IdCarrera == pId);
        }
    }
}
