﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class LaboratoriosBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        public List<LABORATORIO> obtenerLaboratorio()
        {
            return modelo.LABORATORIOS.ToList();
        }

        public int agregarLaboratorio(LABORATORIO pLaboratorio)
        {
            modelo.LABORATORIOS.Add(pLaboratorio);
            return modelo.SaveChanges();
        }

        public LABORATORIO buscarLaboratorio(LABORATORIO pLaboratorio)
        {
            return modelo.LABORATORIOS.SingleOrDefault(p => p.IdLaboratorio == pLaboratorio.IdLaboratorio);
        }

        public int modificarlaboratorio(LABORATORIO pLaboratorio)
        {
            LABORATORIO Laboratorio = buscarLaboratorio(pLaboratorio);

            Laboratorio.Nombre = pLaboratorio.Nombre;
            Laboratorio.Numero = pLaboratorio.Numero;
            Laboratorio.Foto = pLaboratorio.Foto;
            Laboratorio.Ubicacion = pLaboratorio.Ubicacion;
            Laboratorio.CARRERA = pLaboratorio.CARRERA;
            Laboratorio.ALUMNO = pLaboratorio.ALUMNO;

            return modelo.SaveChanges();
        }

        public int eliminarLaboratorio(LABORATORIO pLaboratorio)
        {
            LABORATORIO Laboratorio = buscarLaboratorio(pLaboratorio);
            if (Laboratorio != null)
                modelo.LABORATORIOS.Remove(Laboratorio);

            return modelo.SaveChanges();
        }

        public LABORATORIO obtenerLaboratorioPorId(int pId)
        {
            return modelo.LABORATORIOS.SingleOrDefault(p => p.IdLaboratorio == pId);
        }
    }
}
