﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class DocentesBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        public List<DOCENTE> obtenerDocente()
        {
            return modelo.DOCENTEs.ToList();
        }

        public int agregarDocente(DOCENTE pDocente)
        {
            modelo.DOCENTEs.Add(pDocente);
            return modelo.SaveChanges();
        }

        public DOCENTE buscarDocente(DOCENTE pDocente)
        {
            return modelo.DOCENTEs.SingleOrDefault(p => p.IdDocente == pDocente.IdDocente);
        }

        public int modificarDocente(DOCENTE pDocente)
        {
            DOCENTE Docente = buscarDocente(pDocente);

            Docente.Nombre = pDocente.Nombre;
            Docente.Apellido = pDocente.Apellido;
            Docente.Foto = pDocente.Foto;
           
            return modelo.SaveChanges();
        }

        public int eliminarDocente(DOCENTE pDocente)
        {
            DOCENTE Docente  = buscarDocente(pDocente);
            if (Docente != null)
                modelo.DOCENTEs.Remove(Docente);

            return modelo.SaveChanges();
        }

        public DOCENTE obtenerDocentePorId(int pId)
        {
            return modelo.DOCENTEs.SingleOrDefault(p => p.IdDocente == pId);
        }
    }
}
