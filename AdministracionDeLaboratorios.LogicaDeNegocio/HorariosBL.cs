﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class HorariosBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        public List<HORARIO> obtenerOrario()
        {
            return modelo.HORARIOS.ToList();
        }

        public int agregarHorario(HORARIO pHorario)
        {
            modelo.HORARIOS.Add(pHorario);
            return modelo.SaveChanges();
        }

        public HORARIO buscarHorario(HORARIO pHorario)
        {
            return modelo.HORARIOS.SingleOrDefault(p => p.IdHorario == pHorario.IdHorario);
        }

        public int modificarHorario(HORARIO pHorario)
        {
            HORARIO Horario = buscarHorario(pHorario);

            Horario.Hora = pHorario.Hora;
            Horario.Turno = pHorario.Turno;
            Horario.IdDia = pHorario.IdDia;

            return modelo.SaveChanges();
        }

        public int eliminarHorario(HORARIO pHorario)
        {
            HORARIO Horario = buscarHorario(pHorario);
            if (Horario != null)
                modelo.HORARIOS.Remove(Horario);

            return modelo.SaveChanges();
        }

        public HORARIO obtenerHorarioPorId(int pId)
        {
            return modelo.HORARIOS.SingleOrDefault(p => p.IdHorario == pId);
        }
    }
}
