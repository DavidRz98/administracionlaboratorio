﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class UsuariosBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        public List<USUARIO> obtenerUsuario()
        {
            return modelo.USUARIOS.ToList();
        }
        
        public int agregarUsuario(USUARIO pUsuarios)
        {
            modelo.USUARIOS.Add(pUsuarios);
            return modelo.SaveChanges();
        }

        public USUARIO buscarUsuario(USUARIO pUsuario)
        {
            return modelo.USUARIOS.SingleOrDefault(p => p.IdUsuario == pUsuario.IdUsuario);
        }

        public int modificarUsuario(USUARIO pUsuario)
        {
            USUARIO Usuario = buscarUsuario(pUsuario);

            Usuario.Nombre = pUsuario.Nombre;
            Usuario.Password = pUsuario.Password;

            return modelo.SaveChanges();
        }

        public int eliminarUsuario(USUARIO pUsuario)
        {
            USUARIO Usuario = buscarUsuario(pUsuario);
            if (Usuario != null)
                modelo.USUARIOS.Remove(Usuario);

            return modelo.SaveChanges();
        }

        public USUARIO obtenerUsuarioPorId(int pId)
        {
            return modelo.USUARIOS.SingleOrDefault(p => p.IdUsuario == pId);
        }
    }
}
