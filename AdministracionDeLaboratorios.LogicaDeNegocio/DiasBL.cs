﻿using AdministracionDeLaboratorios.AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.LogicaDeNegocio
{
    public class DiasBL
    {
        ADMINCOMPUTOEntities modelo = BDcomun.contexto;

        public List<DIA> obtenerDia()
        {
            return modelo.DIAS.ToList();
        }

        public int agregarDia(DIA pDia)
        {
            modelo.DIAS.Add(pDia);
            return modelo.SaveChanges();
        }

        public DIA buscarDia(DIA pDia)
        {
            return modelo.DIAS.SingleOrDefault(p => p.IdDia == pDia.IdDia);
        }

        public int modificarDia(DIA pDia)
        {
            DIA Dia = buscarDia(pDia);

            Dia.Nombre = pDia.Nombre;

            return modelo.SaveChanges();
        }

        public int  eliminarDia(DIA pDia)
        {
            DIA Dia = buscarDia(pDia);
            if (Dia != null)
                modelo.DIAS.Remove(Dia);

            return modelo.SaveChanges();
        }
        public DIA obtenerDiaPorId(int pId)
        {
            return modelo.DIAS.SingleOrDefault(p => p.IdDia == pId);
        }
    }
}
