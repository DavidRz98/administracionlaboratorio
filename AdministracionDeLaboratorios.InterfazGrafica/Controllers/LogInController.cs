﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using AdministracionDeLaboratorios.LogicaDeNegocio;
using AdministracionDeLaboratorios.AccesoADatos;


namespace AdministracionDeLaboratorios.InterfazGrafica.Controllers
{
    public class LogInController : Controller
    {
        //// GET: LogIn
        //public ActionResult Index()
        //{
        //    return View();
        //}

        
        CarrerasBL CarreBL = new CarrerasBL();
        DiasBL DiaBL = new DiasBL();
        DocentesBL DocentBL = new DocentesBL();
        HorariosBL HorarBL = new HorariosBL();
        LaboratoriosBL LaborBL = new LaboratoriosBL();


        #region Login

        UsuariosBL UsuBL = new UsuariosBL();
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            return View();
        }
        //accion del login 
        [HttpPost]
        public ActionResult LogIn(string NOM, string PASS)
        {
            List<USUARIO> Lista = UsuBL.obtenerUsuario();

            USUARIO user = Lista.FirstOrDefault(log => log.Nombre == NOM & log.Password == PASS);
            //redirecciona a el menu de el administrador
            if (user != null/* && ROL == "ADMINISTRADOR"*/)
            {
                FormsAuthentication.SetAuthCookie(NOM, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["msg"] = "Los datos son incorrectos";
                return View();
            }


        }
        //accion que redirecciona a el menu
        [Authorize]
        public ActionResult Menu()
        {
            return View();
        }
        //accion para cerrar sesion
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("LogIn", "LogIn");

        }
        #endregion

        #region LoginAlumno
        [AllowAnonymous]
        public ActionResult LogInAlumno()
        {
            return View();
        }

        AlumnosBL alumnoBL = new AlumnosBL();

        [HttpPost]
        public ActionResult LogInAlumno(string carnet)
        {
            List<ALUMNO> Lista = alumnoBL.obtenerAlumno();

            ALUMNO alumno = Lista.FirstOrDefault(log => log.NumCarnet == carnet);
            //redirecciona a el menu de el administrador
            if (alumno != null)
            {
                FormsAuthentication.SetAuthCookie(carnet, false);
                return RedirectToAction("Informacion", "Cliente");
                //return RedirectToAction("Informacion", "Cliente", new { pId = alumno.IdAlumno });
            }
            else
            {
                TempData["msg"] = "Alumno no encontrado";
                return View();
            }
        }
        #endregion
       
    }


}