﻿using AdministracionDeLaboratorios.AccesoADatos;
using AdministracionDeLaboratorios.LogicaDeNegocio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AdministracionDeLaboratorios.InterfazGrafica.Controllers
{
    public class AdministradorController : Controller
    {
        // GET: Administrador

        AlumnosBL AlumBL = new AlumnosBL();
        CarrerasBL CarreBL = new CarrerasBL();
        DiasBL DiaBL = new DiasBL();
        DocentesBL DocentBL = new DocentesBL();
        HorariosBL HorarBL = new HorariosBL();
        LaboratoriosBL LaborBL = new LaboratoriosBL();
        UsuariosBL UsuBL = new UsuariosBL();
       
        #region ALUMNO
        public ActionResult ListadoAlumnos()
        {
            List<ALUMNO> lista = AlumBL.obtenerAlumno();
            return View(lista);
        }

        public ActionResult AgregarAlumno()
        {

            List<CARRERA> listaCarrera = CarreBL.obtenerCarrera();
            ViewBag.Carrera = listaCarrera;

            List<HORARIO> listarHorario = HorarBL.obtenerOrario();
            ViewBag.Horario = listarHorario;

            return View();
        }

        [HttpPost]

        public ActionResult AgregarAlumno(ALUMNO pAlumno, int pCarrera, int pHorario)
        {
            ALUMNO _al = pAlumno;

            _al.HORARIO = HorarBL.obtenerHorarioPorId(pHorario);
            _al.CARRERA = CarreBL.obtenerCarreraPorId(pCarrera);
            if (pAlumno.Archivo != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(pAlumno.Archivo.FileName);
                string extension = Path.GetExtension(pAlumno.Archivo.FileName);
                fileName = fileName + extension;
                pAlumno.Foto = fileName;
                fileName = Path.Combine(Server.MapPath("~/Fotos/"), fileName);
                pAlumno.Archivo.SaveAs(fileName);
            }

            if (AlumBL.agregarAlumno(_al) > 0)
            {
      
                return RedirectToAction("ListadoAlumnos");
            }
            else
            {
                return View();
            }
        }
        public ActionResult VerDetalleAlumno(int pId)
        {
            var alumno = AlumBL.obtenerAlumnoPorId(pId);
            return View(alumno);
        }

        public ActionResult ModificarAlumno(int pId)
        {
            List<CARRERA> listaCarrera = CarreBL.obtenerCarrera();
            ViewBag.Carrera = listaCarrera;

            List<HORARIO> listarHorario = HorarBL.obtenerOrario();
            ViewBag.Horario = listarHorario;

            var alumnos = AlumBL.obtenerAlumnoPorId(pId);
            return View(alumnos);
        }

        [HttpPost]
        public ActionResult ModificarAlumno(ALUMNO pAlumno, int pHorario, int pCarrera )
        {
            ALUMNO _al = pAlumno;

            _al.HORARIO = HorarBL.obtenerHorarioPorId(pHorario);
            _al.CARRERA = CarreBL.obtenerCarreraPorId(pCarrera);
            if (pAlumno.Archivo != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(pAlumno.Archivo.FileName);
                string extension = Path.GetExtension(pAlumno.Archivo.FileName);
                fileName = fileName + extension;
                pAlumno.Foto = fileName;
                fileName = Path.Combine(Server.MapPath("~/Fotos/"), fileName);
                pAlumno.Archivo.SaveAs(fileName);
            }

            if (AlumBL.modificarAlumno(_al) > 0)
            {

                return RedirectToAction("ListadoAlumnos");
            }
            else
            {
                return View();
            }
        }
        public ActionResult EliminarAlumno(int pId)
        {
            List<CARRERA> listaCarrera = CarreBL.obtenerCarrera();
            ViewBag.Carrera = listaCarrera;

            List<HORARIO> listarHorario = HorarBL.obtenerOrario();
            ViewBag.Horario = listarHorario;

            var alumnos = AlumBL.obtenerAlumnoPorId(pId);
            return View(alumnos);
        }

        [HttpPost]
        public ActionResult EliminarAlumno(ALUMNO pAlumno)
        {
            ALUMNO _alumn = pAlumno;

            if (AlumBL.eliminarAlumno(pAlumno) > 0)
            {
                return RedirectToAction("ListadoAlumnos");
            }

            else
            {
                return View(pAlumno);
            }

        }
        #endregion

        #region DOCENTE
        public ActionResult ListadoDocente()
        {
            List<DOCENTE> lista = DocentBL.obtenerDocente();
            return View(lista);
        }
        
        public ActionResult AgregarDocente()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AgregarDocente(DOCENTE pDocente)
        {
           
            DOCENTE _Doc = pDocente;
            
            if (pDocente.Archivo != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(pDocente.Archivo.FileName);
                string extension = Path.GetExtension(pDocente.Archivo.FileName);
                fileName = fileName + extension;
                pDocente.Foto = fileName;
                fileName = Path.Combine(Server.MapPath("~/Fotos/"), fileName);
                pDocente.Archivo.SaveAs(fileName);
            }

            if (DocentBL.agregarDocente(_Doc) > 0)
            {
                return RedirectToAction("ListadoDocente");
            }
            else
            {
                return View();
            }
        }

        public ActionResult VerDetalleDocente(int pId)
        {
            var docente = DocentBL.obtenerDocentePorId(pId);
            return View(docente);
        }

        public ActionResult ModificarDocente(int pId)
        {

            var docente = DocentBL.obtenerDocentePorId(pId);
            return View(docente);
        }

        [HttpPost]
        public ActionResult ModificarDocente(DOCENTE pDocente)
        {
            DOCENTE _Doc = pDocente;
            
            if (pDocente.Archivo != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(pDocente.Archivo.FileName);
                string extension = Path.GetExtension(pDocente.Archivo.FileName);
                fileName = fileName + extension;
                pDocente.Foto = fileName;
                fileName = Path.Combine(Server.MapPath("~/Fotos/"), fileName);
                pDocente.Archivo.SaveAs(fileName);
            }

            if (DocentBL.modificarDocente(_Doc) > 0)
            {

                return RedirectToAction("ListadoDocente");
            }
            else
            {
                return View();
            }
        }

        public ActionResult EliminarDocente(int pId)
        {
            List<DOCENTE> listaDocente = DocentBL.obtenerDocente();
            ViewBag.Docente = listaDocente;

            

            var docente = DocentBL.obtenerDocentePorId(pId);
            return View(docente);
        }

        [HttpPost]
        public ActionResult EliminarDocente(DOCENTE pDocente)
        {
            DOCENTE _docent = pDocente;

            if (DocentBL.eliminarDocente(pDocente) > 0)
            {
                return RedirectToAction("ListadoDocente");
            }

            else
            {
                return View(pDocente);
            }

        }
        #endregion

        #region DIAS
        public ActionResult ListadoDias()
        {
            List<DIA> lista = DiaBL.obtenerDia();
            return View(lista);
        }

        public ActionResult AgregarDia()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AgregarDia(DIA pDia)
        {

            DIA _Dia = pDia;
            
            if (DiaBL.agregarDia(_Dia) > 0)
            {
                return RedirectToAction("ListadoDias");
            }
            else
            {
                return View();
            }
        }

        public ActionResult VerDetalleDia(int pId)
        {
            var dia = DiaBL.obtenerDiaPorId(pId);
            return View(dia);
        }
        
        public ActionResult ModificarDia(int pId)
        {

            var dia = DiaBL.obtenerDiaPorId(pId);
            return View(dia);
        }

        [HttpPost]
        public ActionResult ModificarDia(DIA pDia)
        {
            DIA _Dia = pDia;

            
            if (DiaBL.modificarDia(_Dia) > 0)
            {

                return RedirectToAction("ListadoDias");
            }
            else
            {
                return View();
            }
        }

        public ActionResult EliminarDia(int pId)
        {
            List<DIA> listaDia = DiaBL.obtenerDia();
            ViewBag.Dia = listaDia;

            var dia = DiaBL.obtenerDiaPorId(pId);
            return View(dia);
        }

        [HttpPost]
        public ActionResult EliminarDia(DIA pDia)
        {
            DIA _dia = pDia;

            if (DiaBL.eliminarDia(pDia) > 0)
            {
                return RedirectToAction("ListadoDias");
            }

            else
            {
                return View(pDia);
            }

        }
        
        #endregion

        #region CARRERA
        public ActionResult ListadoCarrera()
        {
            List<CARRERA> lista = CarreBL.obtenerCarrera();
            return View(lista);
        }

        public ActionResult AgregarCarrera()
        {
            List<DOCENTE> listarDocente = DocentBL.obtenerDocente();
            ViewBag.Docente = listarDocente;

            return View();
        }

        [HttpPost]
        public ActionResult AgregarCarrera(CARRERA pCarrera, int pDocente)
        {
            CARRERA _Carre = pCarrera;

            _Carre.DOCENTE = DocentBL.obtenerDocentePorId(pDocente);

            if (CarreBL.agregarCarrera(_Carre) > 0)
            {

                return RedirectToAction("ListadoCarrera");
            }
            else
            {
                return View("ListadoCarrera");
            }
        }
        public ActionResult VerDetalleCarrera(int pId)
        {
            var carrera = CarreBL.obtenerCarreraPorId(pId);
            return View(carrera);
        }
        public ActionResult ModificarCarrera(int pId)
        {
            List<DOCENTE> listaDocente = DocentBL.obtenerDocente();
            ViewBag.Docente = listaDocente;
            
            var carrera = CarreBL.obtenerCarreraPorId(pId);
            return View(carrera);
        }

        [HttpPost]
        public ActionResult ModificarCarrera(CARRERA pCarrera, int pDocente)
        {
            CARRERA _Carre = pCarrera;

            _Carre.DOCENTE = DocentBL.obtenerDocentePorId(pDocente);

            if (CarreBL.modificarCarrera(_Carre) > 0)
            {

                return RedirectToAction("ListadoCarrera");
            }
            else
            {
                return View();
            }
        }

        public ActionResult EliminarCarrera(int pId)
        {
            List<DOCENTE> listaDocente = DocentBL.obtenerDocente();
            ViewBag.Docente = listaDocente;

            var carrera = CarreBL.obtenerCarreraPorId(pId);
            return View(carrera);
        }

        [HttpPost]
        public ActionResult EliminarCarrera(CARRERA pCarrrera)
        {
            CARRERA _carre = pCarrrera;

            if (CarreBL.eliminarCarrera(pCarrrera) > 0)
            {
                return RedirectToAction("ListadoCarrera");
            }

            else
            {
                return View(pCarrrera);
            }

        }
        #endregion

        #region LABORATORIO

        public ActionResult ListadoLaboratorio()
        {
            List<LABORATORIO> lista = LaborBL.obtenerLaboratorio();
            return View(lista);
        }

        public ActionResult AgregarLaboratorio()
        {

            List<ALUMNO> listaAlumno = AlumBL.obtenerAlumno();
            ViewBag.Alumno = listaAlumno;

            List<CARRERA> listarCarrera = CarreBL.obtenerCarrera();
            ViewBag.Carrera = listarCarrera;

            return View();
        }

        [HttpPost]

        public ActionResult AgregarLaboratorio(LABORATORIO pLaboratorio, int pAlumno, int pCarrera)
        {
            LABORATORIO _lab = pLaboratorio;

            _lab.CARRERA = CarreBL.obtenerCarreraPorId(pCarrera);
            _lab.ALUMNO = AlumBL.obtenerAlumnoPorId(pAlumno);

            if (pLaboratorio.Archivo != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(pLaboratorio.Archivo.FileName);
                string extension = Path.GetExtension(pLaboratorio.Archivo.FileName);
                fileName = fileName + extension;
                pLaboratorio.Foto = fileName;
                fileName = Path.Combine(Server.MapPath("~/Fotos/"), fileName);
                pLaboratorio.Archivo.SaveAs(fileName);
            }

            if (LaborBL.agregarLaboratorio(_lab) > 0)
            {

                return RedirectToAction("ListadoLaboratorio");
            }
            else
            {
                return View();
            }
        }
        public ActionResult VerDetalleLaboratorio(int pId)
        {
            var laboratorio = LaborBL.obtenerLaboratorioPorId(pId);
            return View(laboratorio);
        }

        public ActionResult ModificarLaboratorio(int pId)
        {

            List<ALUMNO> listaAlumno = AlumBL.obtenerAlumno();
            ViewBag.Alumno = listaAlumno;

            List<CARRERA> listarCarrera = CarreBL.obtenerCarrera();
            ViewBag.Carrera = listarCarrera;

            var laboratorio = LaborBL.obtenerLaboratorioPorId(pId);
            return View(laboratorio);
        }

        [HttpPost]
        public ActionResult ModificarLaboratorio(LABORATORIO pLaboratorio, int pCarrera, int pAlumno)
        {
            LABORATORIO _lab = pLaboratorio;

            _lab.CARRERA = CarreBL.obtenerCarreraPorId(pCarrera);
            _lab.ALUMNO = AlumBL.obtenerAlumnoPorId(pAlumno);
            
            if (pLaboratorio.Archivo != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(pLaboratorio.Archivo.FileName);
                string extension = Path.GetExtension(pLaboratorio.Archivo.FileName);
                fileName = fileName + extension;
                pLaboratorio.Foto = fileName;
                fileName = Path.Combine(Server.MapPath("~/Fotos/"), fileName);
                pLaboratorio.Archivo.SaveAs(fileName);
            }

            if (LaborBL.modificarlaboratorio(_lab) > 0)
            {

                return RedirectToAction("ListadoLaboratorio");
            }
            else
            {
                return View();
            }
        }
        public ActionResult EliminarLaboratorio(int pId)
        {
            List<CARRERA> listaCarrera = CarreBL.obtenerCarrera();
            ViewBag.Carrera = listaCarrera;

            List<ALUMNO> listarAlumno = AlumBL.obtenerAlumno();
            ViewBag.Alumno = listarAlumno;

            var laboratorio = LaborBL.obtenerLaboratorioPorId(pId);
            return View(laboratorio);
        }

        [HttpPost]
        public ActionResult EliminarLaboratorio(LABORATORIO pLaboratorio)
        {
            LABORATORIO _lab = pLaboratorio;

            if (LaborBL.eliminarLaboratorio(pLaboratorio) > 0)
            {
                return RedirectToAction("ListadoLaboratorio");
            }

            else
            {
                return View(pLaboratorio);
            }

        }

        #endregion

        #region USUARIO
        public ActionResult ListadoUsuario()
        {
            List<USUARIO> lista = UsuBL.obtenerUsuario();
            return View(lista);
        }

        public ActionResult AgregarUsuario()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AgregarUsuario(USUARIO pUsuario)
        {

            USUARIO _Usu = pUsuario;

            if (UsuBL.agregarUsuario(_Usu) > 0)
            {
                return RedirectToAction("ListadoUsuario");
            }
            else
            {
                return View();
            }
        }

        public ActionResult VerDetalleUsuario(int pId)
        {
            var usuario = UsuBL.obtenerUsuarioPorId(pId);
            return View(usuario);
        }

        public ActionResult ModificarUsuario(int pId)
        {

            var usuario = UsuBL.obtenerUsuarioPorId(pId);
            return View(usuario);
        }

        [HttpPost]
        public ActionResult ModificarUsuario(USUARIO pUsuario)
        {
            USUARIO _Usuario = pUsuario;


            if (UsuBL.modificarUsuario(_Usuario) > 0)
            {

                return RedirectToAction("ListadoUsuario");
            }
            else
            {
                return View();
            }
        }

        public ActionResult EliminarUsuario(int pId)
        {
            List<USUARIO> listaUsuario = UsuBL.obtenerUsuario();
            ViewBag.Usuario = listaUsuario;

            var usuario = UsuBL.obtenerUsuarioPorId(pId);
            return View(usuario);
        }

        [HttpPost]
        public ActionResult EliminarUsuario(USUARIO pUsuario)
        {
            USUARIO _usuario = pUsuario;

            if (UsuBL.eliminarUsuario(_usuario) > 0)
            {
                return RedirectToAction("ListadoUsuario");
            }

            else
            {
                return View(pUsuario);
            }

        }

        #endregion

        //#region LOGIN
        //public ActionResult LogIn()
        //{
        //    return View();
        //}

        ////accion del login 
        //[HttpPost]

        //public ActionResult LogIn(string NOM, string PASS, string ROL)
        //{
        //    List<USUARIO> Lista = UsuBL.obtenerUsuario();

        //    USUARIO user = Lista.FirstOrDefault(log => log.Nombre == NOM & log.Password == PASS);
        //    //redirecciona a el menu de el administrador
        //    if (user != null/* && ROL == "ADMINISTRADOR"*/)
        //    {
        //        FormsAuthentication.SetAuthCookie(NOM, false);
        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //    {
        //        TempData["msg"] = "Los datos son incorrectos";
        //        return View();
        //    }


        //}
        ////accion que redirecciona a el menu
        //[Authorize]
        //public ActionResult Menu()
        //{
        //    return View();
        //}
        ////accion para cerrar sesion
        //public ActionResult Logout()
        //{
        //    FormsAuthentication.SignOut();
        //    Session.Abandon();
        //    return RedirectToAction("LogIn", "Administrador");
        //}
        //#endregion
    }
}