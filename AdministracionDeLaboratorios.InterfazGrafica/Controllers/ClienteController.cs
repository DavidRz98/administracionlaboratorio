﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using AdministracionDeLaboratorios.LogicaDeNegocio;
using AdministracionDeLaboratorios.AccesoADatos;
using AdministracionDeLaboratorios.InterfazGrafica;
namespace AdministracionDeLaboratorios.InterfazGrafica.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente

        AlumnosBL AlumBL = new AlumnosBL();
        CarrerasBL CarreBL = new CarrerasBL();
        DiasBL DiaBL = new DiasBL();
        DocentesBL DocentBL = new DocentesBL();
        HorariosBL HorarBL = new HorariosBL();
        LaboratoriosBL LaborBL = new LaboratoriosBL();
        UsuariosBL UsuBL = new UsuariosBL();


        public ActionResult Informacion()
        {
            return View();
        }

        #region Docente

        public ActionResult ListadoDocente()
        {
            List<DOCENTE> lista = DocentBL.obtenerDocente();
            return View(lista);
        }

        public ActionResult VerDetalleDocente(int pId)
        {
            var docente = DocentBL.obtenerDocentePorId(pId);
            return View(docente);
        }

        #endregion

        #region Carrera
        public ActionResult ListadoCarrera()
        {
            List<CARRERA> lista = CarreBL.obtenerCarrera();
            return View(lista);
        }
        public ActionResult VerDetalleCarrera(int pId)
        {
            var carrera = CarreBL.obtenerCarreraPorId(pId);
            return View(carrera);
        }
        #endregion

        #region Laboratorio

        public ActionResult ListadoLaboratorio()
        {
            List<LABORATORIO> lista = LaborBL.obtenerLaboratorio();
            return View(lista);
        }

        public ActionResult VerDetalleLaboratorio(int pId)
        {
            var laboratorio = LaborBL.obtenerLaboratorioPorId(pId);
            return View(laboratorio);
        }

        #endregion
    }
}