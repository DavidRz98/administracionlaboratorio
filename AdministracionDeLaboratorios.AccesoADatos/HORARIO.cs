//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdministracionDeLaboratorios.AccesoADatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class HORARIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HORARIO()
        {
            this.ALUMNOS = new HashSet<ALUMNO>();
        }
    
        public int IdHorario { get; set; }
        public System.TimeSpan Hora { get; set; }
        public string Turno { get; set; }
        public int IdDia { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALUMNO> ALUMNOS { get; set; }
        public virtual DIA DIA { get; set; }
    }
}
