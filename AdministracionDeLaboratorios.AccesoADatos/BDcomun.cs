﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministracionDeLaboratorios.AccesoADatos
{
    public static class BDcomun
    {
        static ADMINCOMPUTOEntities _contexto;
        public static ADMINCOMPUTOEntities contexto
        {
            get
            {
                if (_contexto == null)
                    _contexto = new ADMINCOMPUTOEntities();
                return _contexto;
            }
        }

    }
}
