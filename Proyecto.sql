USE [ADMINCOMPUTO]
GO
/****** Object:  Table [dbo].[ALUMNOS]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ALUMNOS](
	[IdAlumno] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[NumCarnet] [varchar](15) NOT NULL,
	[Foto] [varchar](500) NOT NULL,
	[Edad] [int] NOT NULL,
	[FechaIngreso] [date] NOT NULL,
	[Direccion] [varchar](100) NOT NULL,
	[IdHorario] [int] NOT NULL,
	[IdCarrera] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdAlumno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CARRERAS]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CARRERAS](
	[IdCarrera] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[IdDocente] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCarrera] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DIAS]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DIAS](
	[IdDia] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DOCENTE]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DOCENTE](
	[IdDocente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Foto] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDocente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HORARIOS]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HORARIOS](
	[IdHorario] [int] IDENTITY(1,1) NOT NULL,
	[Hora] [time](7) NOT NULL,
	[Turno] [varchar](10) NOT NULL,
	[IdDia] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdHorario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LABORATORIOS]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LABORATORIOS](
	[IdLaboratorio] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Numero] [int] NOT NULL,
	[Foto] [varchar](500) NOT NULL,
	[Ubicacion] [varchar](100) NOT NULL,
	[IdCarrera] [int] NOT NULL,
	[IdAlumno] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLaboratorio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[USUARIOS]    Script Date: 08/12/2019 21:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USUARIOS](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Password] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ALUMNOS] ON 

INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (1, N'Lupesss', N'Ramirez Puente', N'2120122018', N'eli.jpg', 18, CAST(N'2020-01-01' AS Date), N'San salvador', 2, 2)
INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (2, N'Jose Antnonio ', N'Ramirez ', N'21-2222-2016', N'docentes.jpg', 21, CAST(N'2016-01-01' AS Date), N'San salvador', 2, 5)
INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (4, N'Josseline Yamilet2121', N'Hernandez Mangivar', N'23-0311-2014', N'34sadsa.PNG', 24, CAST(N'2014-02-10' AS Date), N'Apopa-San salvador', 1, 2)
INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (5, N'Carlos Antonio', N'Hernandez', N'23-0311-2012', N'Captura.PNG', 23, CAST(N'2019-11-02' AS Date), N'San Juan Opcio La Libertad', 2, 12)
INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (6, N'Julia Yamileth', N'Lozano Ramirez', N'23-0311-2011', N'Captura2.PNG', 23, CAST(N'2012-01-01' AS Date), N'San Juan- LA Achadura', 1, 11)
INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (10, N'asdasdsa', N'asdasdsa', N'25-0591-2017', N'Captura4.PNG', 21, CAST(N'2019-01-01' AS Date), N'sdada', 1, 14)
INSERT [dbo].[ALUMNOS] ([IdAlumno], [Nombre], [Apellido], [NumCarnet], [Foto], [Edad], [FechaIngreso], [Direccion], [IdHorario], [IdCarrera]) VALUES (11, N'LESLIE', N'RIVAS', N'25-1590-2016', N'Captura45.PNG', 22, CAST(N'2016-01-01' AS Date), N'San Salvador', 1, 2)
SET IDENTITY_INSERT [dbo].[ALUMNOS] OFF
SET IDENTITY_INSERT [dbo].[CARRERAS] ON 

INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (2, N'Otros', 1)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (5, N'Ingenieria de Sistemas', 4)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (6, N'Derecho', 1)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (9, N'Ingenieria Industrial', 9)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (10, N'Inegnieria Mecanica', 4)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (11, N'Licenciatura en Mercadeo', 1)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (12, N'Ingenieria en Base de Datos', 1)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (13, N'Tecnico de Ingenieria en desarrollo de software', 4)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (14, N'Ingenieria Ambiental', 4)
INSERT [dbo].[CARRERAS] ([IdCarrera], [Nombre], [IdDocente]) VALUES (15, N'Otros', 1)
SET IDENTITY_INSERT [dbo].[CARRERAS] OFF
SET IDENTITY_INSERT [dbo].[DIAS] ON 

INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (1, N'Lunes')
INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (2, N'Martes')
INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (3, N'Miercoles')
INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (4, N'Jueves')
INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (5, N'Viernes')
INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (6, N'Sabado')
INSERT [dbo].[DIAS] ([IdDia], [Nombre]) VALUES (9, N'Domingo')
SET IDENTITY_INSERT [dbo].[DIAS] OFF
SET IDENTITY_INSERT [dbo].[DOCENTE] ON 

INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (1, N'Otros', N'Otros', N'fondo2.jpg')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (4, N'Carlos Arteaga', N'Ramirez Gonzales', N'Captura.PNG')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (5, N'Jazmin Guadalupe', N'Ramirez Puente', N'Captura2.PNG')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (6, N'Priscila', N'Zuna', N'eli.jpg')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (9, N'Rebeca Carolina', N'Diaz Hernandez', N'eli.jpg')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (10, N'Carlos Antonio', N'Escobar Aguilera', N'Captura.PNG')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (11, N'Jose Antonio', N'Guzman Alvarado', N'Captura3.PNG')
INSERT [dbo].[DOCENTE] ([IdDocente], [Nombre], [Apellido], [Foto]) VALUES (12, N'JzRAmirez98', N'fghfg', N'Captura2.PNG')
SET IDENTITY_INSERT [dbo].[DOCENTE] OFF
SET IDENTITY_INSERT [dbo].[HORARIOS] ON 

INSERT [dbo].[HORARIOS] ([IdHorario], [Hora], [Turno], [IdDia]) VALUES (1, CAST(N'08:00:00' AS Time), N'Mañana', 1)
INSERT [dbo].[HORARIOS] ([IdHorario], [Hora], [Turno], [IdDia]) VALUES (2, CAST(N'08:00:00' AS Time), N'Tarde', 2)
SET IDENTITY_INSERT [dbo].[HORARIOS] OFF
SET IDENTITY_INSERT [dbo].[LABORATORIOS] ON 

INSERT [dbo].[LABORATORIOS] ([IdLaboratorio], [Nombre], [Numero], [Foto], [Ubicacion], [IdCarrera], [IdAlumno]) VALUES (11, N'Laboratorio de Ciencias', 6, N'263-centro-de-computo-mac.jpg', N'Frente a edificio b', 14, 2)
INSERT [dbo].[LABORATORIOS] ([IdLaboratorio], [Nombre], [Numero], [Foto], [Ubicacion], [IdCarrera], [IdAlumno]) VALUES (12, N'Laboratorio de Mecanica2', 21, N'img-1517585647.jpg', N'pasillo2', 2, 1)
INSERT [dbo].[LABORATORIOS] ([IdLaboratorio], [Nombre], [Numero], [Foto], [Ubicacion], [IdCarrera], [IdAlumno]) VALUES (1012, N'Laboratorio de Informatica', 11, N'Centros-de-Computo-02-1024x618.jpg', N'pasillo 22', 5, 1)
INSERT [dbo].[LABORATORIOS] ([IdLaboratorio], [Nombre], [Numero], [Foto], [Ubicacion], [IdCarrera], [IdAlumno]) VALUES (1014, N'sd', 2, N'263-centro-de-computo-mac.jpg', N'sad', 6, 2)
INSERT [dbo].[LABORATORIOS] ([IdLaboratorio], [Nombre], [Numero], [Foto], [Ubicacion], [IdCarrera], [IdAlumno]) VALUES (1015, N'qweqwed', 2, N'img-1517585647.jpg', N'asdsda', 6, 2)
SET IDENTITY_INSERT [dbo].[LABORATORIOS] OFF
SET IDENTITY_INSERT [dbo].[USUARIOS] ON 

INSERT [dbo].[USUARIOS] ([IdUsuario], [Nombre], [Password]) VALUES (2, N'DAVID', N'212914')
INSERT [dbo].[USUARIOS] ([IdUsuario], [Nombre], [Password]) VALUES (4, N'JVILLEDA', N'123')
INSERT [dbo].[USUARIOS] ([IdUsuario], [Nombre], [Password]) VALUES (5, N'LESLIE', N'123')
INSERT [dbo].[USUARIOS] ([IdUsuario], [Nombre], [Password]) VALUES (6, N'GTORRES', N'123')
SET IDENTITY_INSERT [dbo].[USUARIOS] OFF
ALTER TABLE [dbo].[ALUMNOS]  WITH CHECK ADD  CONSTRAINT [R_CARRERAS] FOREIGN KEY([IdCarrera])
REFERENCES [dbo].[CARRERAS] ([IdCarrera])
GO
ALTER TABLE [dbo].[ALUMNOS] CHECK CONSTRAINT [R_CARRERAS]
GO
ALTER TABLE [dbo].[ALUMNOS]  WITH CHECK ADD  CONSTRAINT [R_HORARIOS] FOREIGN KEY([IdHorario])
REFERENCES [dbo].[HORARIOS] ([IdHorario])
GO
ALTER TABLE [dbo].[ALUMNOS] CHECK CONSTRAINT [R_HORARIOS]
GO
ALTER TABLE [dbo].[CARRERAS]  WITH CHECK ADD  CONSTRAINT [R_DOCENTE] FOREIGN KEY([IdDocente])
REFERENCES [dbo].[DOCENTE] ([IdDocente])
GO
ALTER TABLE [dbo].[CARRERAS] CHECK CONSTRAINT [R_DOCENTE]
GO
ALTER TABLE [dbo].[HORARIOS]  WITH CHECK ADD  CONSTRAINT [R_DIAS] FOREIGN KEY([IdDia])
REFERENCES [dbo].[DIAS] ([IdDia])
GO
ALTER TABLE [dbo].[HORARIOS] CHECK CONSTRAINT [R_DIAS]
GO
ALTER TABLE [dbo].[LABORATORIOS]  WITH CHECK ADD  CONSTRAINT [R_ALUMNO] FOREIGN KEY([IdAlumno])
REFERENCES [dbo].[ALUMNOS] ([IdAlumno])
GO
ALTER TABLE [dbo].[LABORATORIOS] CHECK CONSTRAINT [R_ALUMNO]
GO
ALTER TABLE [dbo].[LABORATORIOS]  WITH CHECK ADD  CONSTRAINT [R_CARRERA] FOREIGN KEY([IdCarrera])
REFERENCES [dbo].[CARRERAS] ([IdCarrera])
GO
ALTER TABLE [dbo].[LABORATORIOS] CHECK CONSTRAINT [R_CARRERA]
GO
